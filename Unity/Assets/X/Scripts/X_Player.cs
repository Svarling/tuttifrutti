﻿using UnityEngine;
using System.Collections;

public class X_Player : MonoBehaviour {

	public float Speed = 10;
	private Vector3 forceVelocity = new Vector3 (0,600,0);


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKey (KeyCode.LeftArrow) )
			transform.Translate (new Vector3 (1,0,0) * (Time.deltaTime*Speed));

		if (Input.GetKey (KeyCode.RightArrow) )
			transform.Translate ( new Vector3 (-1,0,0)* (Time.deltaTime*Speed));

		if (Input.GetKeyDown (KeyCode.UpArrow) )
			rigidbody.AddForce (forceVelocity);

	}
}
