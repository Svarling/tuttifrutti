﻿Lesson 01
---------

MÅL
* Skapa en box
* Flytta boxen i sidled med piltangenterna
* Få boxen att hoppa med uppåtpil
* Koppla hastighet till en variabel

1. VARIABLER
------------------------

För att skapa en variabel sätter man egentligen ihop olika delar.

A B C = D;
t.ex. public float speed = 1.0f;

A: (typ tillgänglighet mellan scripten och i scriptet)
public (variabeln går att pilla på i unityinspektorn)
private (variabeln är menad bara för detta scriptet)
static public (variabeln kan nås överallt i alla funktioner. Mer om detta senare :)

B: (variabelns typ)
int (heltal, t.ex. 0, 1, 2, 3, 4...)
float (decimaltal eller heltal, t.ex. 0, 0.5f, 1.0f, 2, 3.7421f)
Vector2 (decimaltal i två ledder x och y... t.ex. (0, 1) eller (3.5f, 499.0f)
Vector3 (decimaltal i tre ledder x, y, z... t.ex. (0, 0, 0) eller (100, 101.5f, 0)
bool (används för att säga om något är sant eller falskt. kan alltså vara true eller false som värde. T.ex. om man hoppar så kan man sätta isJumping = true)
string (text, ord, bokstäver... används för att spara namn, eller innehåll i menytexter osv)

C: (namn på variabeln)
speed (om bara ett ord skrivs med små bokstäver)
spaceshipSpeed (om två ord brukar man skriva andra ordet med stor bokstav)

D: (förbestämt värde, avslutas med semikolon)
private int myInt = 5;
private float myFloat = 5.0f;
private Vector3 myVector = new Vector3(0,0,0);
private bool isJumping = false;

2. IF-SATSER
------------------------

bla bla


