﻿using UnityEngine;
using System.Collections;

public class J_Player : MonoBehaviour {

	public float speedX = 1;
	public float jumpForce = 100;

	void Update () 
	{
		if(Input.GetKey (KeyCode.LeftArrow))
		   transform.Translate (new Vector3(-speedX, 0, 0) * Time.deltaTime);
		
		if(Input.GetKey (KeyCode.RightArrow))
		   transform.Translate (new Vector3(speedX, 0, 0) * Time.deltaTime);

		if(Input.GetKeyDown (KeyCode.UpArrow))
		   rigidbody.AddForce (new Vector3(0, jumpForce, 0));
	}
}
